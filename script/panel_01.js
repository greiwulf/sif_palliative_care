(function(){
    var p = [517.52,157.75];
    var r1 = 'rotate(30,'+p[0]+','+p[1]+')';
    var r2 = 'rotate(45,'+p[0]+','+p[1]+')';
    var r3 = 'rotate(90,'+p[0]+','+p[1]+')';
    var dur = 500;

    var svg = d3.select('#svg-01');
    var card = svg.select('#ouch_1_');
    var ap = svg.select('#aeroplane_1_');

    var z = svg.selectAll('g.z');

    card.call(cycle)
        .on('click', click);  


    var c = 0;
    function cycle(d) {
        c++;
        card.transition()
            .duration(dur)
            .ease(d3.easeSin)
            .attrTween("transform", function(){
                return d3.interpolateString( (c%2==0)?r1:r2, (c%2==0)?r2:r1 );
            } )
            .on('end', cycle);
    }

    function click(){

        var r0 = card.attr('transform');

        card
            .interrupt()
            .transition()
            .duration(2000)
            .ease(d3.easeBounce)
            .attrTween("transform", function(){
                return d3.interpolateString( r0, r3 );
            } )
            .on('end', cycle)
            ;
    }

    var apTarget = {x:535,y:375};
    console.log(ap);
    ap.call(cycleAp);
    function cycleAp(d) {
        d.attr('transform','translate(0,0)');
        d.transition()
            .duration(1500)
            .ease(d3.easeSin)
            .attr('transform','translate(75,-75)')
            .on('end', function(){cycleAp(d)});
    }

    z.select('path').on('click', click2)
    console.log(z.select('path'));
    function click2(d){
        console.log('d');
        

        d
            .transition()
            .duration(2000)
            .ease(d3.easeBounce)
            .attrTween("transform", function(){
                return d3.interpolateString( r0, r3 );
            } )
            
            ;
    }


})();